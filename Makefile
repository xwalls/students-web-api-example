build:
	docker build \
	--tag students-web-api-example \
	.

run:
	docker run \
		--interactive \
		--name students-web-api-example \
		--publish 8181:80 \
		--rm \
		students-web-api-example

stop:
	docker stop students-web-api-example
	