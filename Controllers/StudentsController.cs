using System.Collections.Generic;
using ExcelDataReader;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace students_web_api_example.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StudentsController : ControllerBase
    {
        private readonly ILogger<StudentsController> _logger;
        private SchoolRecords _school_records;

        public StudentsController(ILogger<StudentsController> logger)
        {
            _logger = logger;
        }

        [HttpPost("upload-file")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            List<StudentItem> students = new List<StudentItem>();
            if (CheckIfExcelFile(file))
            {
                await WriteFile(file);
                students = this.ReadFile(file);
            }
            else
            {
                return BadRequest(new { message = "Invalid file extension" });
            }

            string jsonString = JsonSerializer.Serialize(
                new { 
                    students = students, 
                    best_student = this._school_records.getStudentWithBestScore(),
                    worst_student = this._school_records.getStudentWithWorstScore(),
                    general_score = this._school_records.getSchoolScore()
                });

            return Ok(jsonString);
        }

        private bool CheckIfExcelFile(IFormFile file)
        {
            var extension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
            return (extension == ".xlsx");
        }
        private async Task<bool> WriteFile(IFormFile file)
        {
            bool isSaveSuccess = false;
            string fileName;
            try
            {
                fileName = "Calificaciones.xlsx";
                var pathBuilt = Path.Combine(Directory.GetCurrentDirectory(), "uploads");

                if (!Directory.Exists(pathBuilt))
                {
                    Directory.CreateDirectory(pathBuilt);
                }

                var path = Path.Combine(Directory.GetCurrentDirectory(), "uploads",
                   fileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                isSaveSuccess = true;
            }
            catch (Exception e)
            {
                _logger.LogInformation($"{e}");
            }

            return isSaveSuccess;
        }

        private List<StudentItem> ReadFile(IFormFile file)
        {
            DataSet dsexcelRecords = new DataSet();
            IExcelDataReader reader = null;
            List<StudentItem> students_list = new List<StudentItem>();

            if (file.FileName.EndsWith(".xlsx"))
                using (var stream2 = new FileStream("./uploads/Calificaciones.xlsx", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    reader = ExcelReaderFactory.CreateOpenXmlReader(stream2);
                    dsexcelRecords = reader.AsDataSet();
                    reader.Close();
                }

            if (dsexcelRecords != null && dsexcelRecords.Tables.Count > 0)
            {
                DataTable students_data_table = dsexcelRecords.Tables[0];
                SchoolRecords school_records = new SchoolRecords(students_data_table);
                this._school_records = school_records;
                students_list = school_records.getStudentsList();
            }
            return students_list;
        }
    }
}
