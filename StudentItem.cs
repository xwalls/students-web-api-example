namespace students_web_api_example
{
    public class StudentItem
    {
        public int id { get; set; }
        public string user_key { get; set; }
        public string name { get; set; }
        public string motherLastName { get; set; }
        public string fatherLastName { get; set; }
        public string lastName { get; set; }
        public string birthday { get; set; }
        public int age { get; set; }
        public int grade { get; set; }
        public string group { get; set; }
        public float score { get; set; }
    }
}