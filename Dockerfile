FROM mcr.microsoft.com/dotnet/aspnet:5.0-focal AS base
WORKDIR /app
EXPOSE 80


FROM mcr.microsoft.com/dotnet/sdk:5.0-focal AS build
WORKDIR /src
COPY ["students-web-api-example.csproj", "./"]
RUN dotnet restore "./students-web-api-example.csproj"
RUN dotnet add package ExcelDataReader --version 3.6.0
COPY . .
WORKDIR "/src/."
RUN dotnet build "students-web-api-example.csproj" -c Release -o /app/build


FROM build AS publish
RUN dotnet publish "students-web-api-example.csproj" -c Release -o /app/publish
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "students-web-api-example.dll"]