using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace students_web_api_example
{
    public class SchoolRecords
    {
        private List<StudentItem> students = new List<StudentItem>();
        private float schoolScore;

        public SchoolRecords(DataTable students_data_table)
        {
            this.buildStudentsList(students_data_table);
            this.calculateSchoolScore();
        }

        private void buildStudentsList(DataTable students_data_table)
        {
            int total_rows = students_data_table.Rows.Count;
            for (int row_index = 1; row_index < total_rows; row_index++)
            {
                StudentItem student = new StudentItem();
                student.id = row_index;
                student.name = Convert.ToString(students_data_table.Rows[row_index][0]);
                student.motherLastName = Convert.ToString(students_data_table.Rows[row_index][1]);
                student.fatherLastName = Convert.ToString(students_data_table.Rows[row_index][2]);
                student.birthday = Convert.ToString(students_data_table.Rows[row_index][3]);
                student.grade = Convert.ToInt32(students_data_table.Rows[row_index][4]);
                student.group = Convert.ToString(students_data_table.Rows[row_index][5]);
                student.score = Convert.ToSingle(students_data_table.Rows[row_index][6]);

                student.lastName = $"{student.fatherLastName} {student.motherLastName}";
                student.age = this.calculateAge(student);
                student.user_key = this.generateUserKey(student);
                this.students.Add(student);

            }
        }

        public List<StudentItem> getStudentsList() {
            return this.students;
        }
        public float getSchoolScore()
        {
            return this.schoolScore;
        }

        public StudentItem getStudentWithBestScore() {
            return this.students.OrderByDescending(student => student.score).First();
        }

        public StudentItem getStudentWithWorstScore() {
            return this.students.OrderByDescending(student => student.score).Last();
        }

        private void calculateSchoolScore() {
            float sum_scores = 0;
            foreach (var student in this.students)
            {
                sum_scores += student.score;
            }
            this.schoolScore = sum_scores / this.students.Count;
        }

        private int calculateAge(StudentItem student)
        {
            DateTime Now = DateTime.Now;
            DateTime parsed_birthday = DateTime.ParseExact(student.birthday, "dd/MM/yyyy", null);
            DateTime student_birthdate = Convert.ToDateTime(parsed_birthday);
            int years_age = new DateTime(DateTime.Now.Subtract(student_birthdate).Ticks).Year - 1;
            return years_age;
        }

        private string generateUserKey(StudentItem student)
        {
            string name_two_chars = student.name.Substring(0, 2).ToUpper();
            string mother_lastname_two_chars = student.motherLastName[^2..].ToUpper();
            string user_key = move_n_spaces_back(name_two_chars) +
                                move_n_spaces_back(mother_lastname_two_chars) +
                                student.age;
            return user_key;
        }

        private string move_n_spaces_back(string name, int spaces = 3)
        {
            char[] alphabet = "ABCDEFGHIJKLMN�OPQRSTUVWXYZ".ToArray();
            string new_name_chars = "";
            foreach (char c in name)
            {
                int position = Array.IndexOf(alphabet, c);
                int new_position = position - spaces;
                if (new_position < 0)
                {
                    new_position = alphabet.Length - (-new_position);
                }
                new_name_chars += alphabet[new_position];
            }
            return new_name_chars;
        }
    }
}